<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Jernlib_Controller Class
 */
class Jernlib_Controller extends CI_Controller
{
    /**
     * The role associated with user.
     * '*' all user
     * '@' logged in user
     * 'Admin' for admin
     * 'Author' for author group
     *
     * @var string
     */
    protected $access = "*";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->authenticate();
    }

    public function authenticate()
    {
        if ($this->access != "*") {
            // here we check the role of the user
            if (! $this->authorize()) {
                die("<h4>Access denied</h4>");
            }

            // if user try to access logged in page check does he/she has
            // logged in if not, redirect to login page
            if (! $this->session->userdata("logged_in")) {
                redirect("auth/login");
            }
        }
    }

    public function authorize()
    {
        if ($this->access == "@") {
            return true;
        } else {
            $access = is_array($this->access) ?
                $this->access :
                explode(",", $this->access);
            if (in_array($this->session->userdata("role"), array_map("trim", $access))) {
                return true;
            }

            return false;
        }
    }
}
