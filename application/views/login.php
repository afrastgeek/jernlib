<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Jernlib login page.">
    <meta name="author" content="M Ammar Fadhlur Rahman">
    <meta name="keyword" content="Jernlib Login">

    <title>Login - Jernlib</title>

    <!-- Icons -->
    <link href="<?php echo base_url('css/simple-line-icons.min.css'); ?>" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="<?php echo base_url('css/style.min.css'); ?>" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card-group mb-0">
                    <div class="card p-2">
                        <div class="card-block">
                            <h1>Login</h1>
                            <p class="text-muted">Sign In to Jernlib</p>
                            <?php $error = $this->session->flashdata("error"); ?>
                            <div class="alert alert-<?php echo $error ? 'warning' : 'info' ?> alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <?php echo $error ? $error : 'Enter your username and password' ?>
                            </div>
                            <?php echo form_open(); ?>
                                <?php $error = form_error("username", "<p class='text-danger'>", '</p>'); ?>
                                <div class="input-group mb-1">
                                    <span class="input-group-addon"><i class="icon-user"></i>
                                    </span>
                                    <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo set_value("username") ?>">
                                </div>
                                <?php echo $error; ?>
                                <?php $error = form_error("password", "<p class='text-danger'>", '</p>'); ?>
                                <div class="input-group mb-2">
                                    <span class="input-group-addon"><i class="icon-lock"></i>
                                    </span>
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                                <?php echo $error; ?>
                                <div class="row">
                                    <div class="col-6">
                                        <input type="submit" class="btn btn-primary px-2" value="Login"></button>
                                    </div>
                                    <div class="col-6 text-right">
                                        <button type="button" class="btn btn-link px-0">Forgot password?</button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <div class="card card-inverse card-primary py-3 hidden-md-down" style="width:44%">
                        <div class="card-block text-center">
                            <div>
                                <h2>Sign up</h2>
                                <p>Explore and share knowledge in limitless library.</p>
                                <a href="<?php echo base_url('auth/register'); ?>" class="btn btn-primary active mt-1">
                                    Register Now!
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap and necessary plugins -->
    <script src="<?php echo base_url('js/libs/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/libs/tether.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/libs/bootstrap.min.js'); ?>"></script>



</body>

</html>
