<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Jernlib Dashboard">
<meta name="author" content="M Ammar Fadhlur Rahman">
<title><?php if (isset($title)) echo "$title | "; ?>Jernlib Dashboard</title>
<!-- Icons -->
<link href="<?php echo base_url('css/simple-line-icons.min.css'); ?>" rel="stylesheet">
<!-- Main styles for this application -->
<link href="<?php echo base_url('css/style.min.css'); ?>" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<header class="app-header navbar">
  <button class="navbar-toggler mobile-sidebar-toggler hidden-lg-up" type="button">☰</button>
  <a class="navbar-brand" href="<?php echo base_url(); ?>">Jernlib</a>
  <ul class="nav navbar-nav hidden-md-down">
    <li class="nav-item">
      <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
    </li>
  </ul>
</header>
<div class="app-body">
  <div class="sidebar">
    <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('dashboard'); ?>"><i class="icon-speedometer"></i> Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('manage/user'); ?>"><i class="icon-people"></i> Users</a>
        </li>
      </ul>
    </nav>
  </div>
  <!-- Main content -->
  <main class="main">
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="breadcrumb-item active"><?php if (isset($title)) echo $title; ?></li>
    </ol>
    <div class="container-fluid">
        <?php if (isset($title)) echo "<h1>$title</h1>"; ?>
