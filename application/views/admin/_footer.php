<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
  </main>
</div>
<footer class="app-footer">
  <a href="<?php echo base_url(''); ?>">Jernlibs</a> © 2017.
  <span class="float-right">Powered by <a href="http://coreui.io">CoreUI</a>
    </span>
</footer>
<!-- Bootstrap and necessary plugins -->
<script src="<?php echo base_url('js/libs/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('js/libs/tether.min.js'); ?>"></script>
<script src="<?php echo base_url('js/libs/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('js/libs/pace.min.js'); ?>"></script>
<!-- GenesisUI main scripts -->
<script src="<?php echo base_url('js/app.js'); ?>"></script>
</body>

</html>
