<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
        <table class="table table-hover table-outline mb-0 hidden-sm-down">
          <thead class="thead-default">
            <tr>
              <th class="text-center"><i class="icon-people"></i>
              </th>
              <th>User</th>
              <th class="text-center">username</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($users as $value): ?>
            <tr>
              <td class="text-center">
                <div class="avatar">
                  <img src="https://www.gravatar.com/avatar/<?php echo md5(strtolower(trim($value->email))); ?>" class="img-avatar" alt="<?php echo $value->email; ?>">
                </div>
              </td>
              <td>
                <div><?php echo $value->name; ?></div>
                <div class="small text-muted">
                  Registered: <?php echo $value->created_at; ?>
                </div>
              </td>
              <td>
                  <div class="text-center"><?php echo $value->username; ?></div>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
    </div>
    <!-- /.conainer-fluid -->
