<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Jernlib | Explore and share knowledge in limitless library</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
    <meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
    <meta name="author" content="FREEHTML5.CO" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,600,400italic,700' rel='stylesheet' type='text/css'>

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?php echo base_url('css/landing/animate.css'); ?>">
    <!-- Flexslider -->
    <link rel="stylesheet" href="<?php echo base_url('css/landing/flexslider.css'); ?>">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?php echo base_url('css/landing/icomoon.css'); ?>">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url('css/landing/magnific-popup.css'); ?>">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?php echo base_url('css/landing/bootstrap.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('css/landing/style.css'); ?>">

    <!-- Modernizr JS -->
    <script src="<?php echo base_url('js/landing/modernizr-2.6.2.min.js'); ?>"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url('js/landing/respond.min.js'); ?>"></script>
    <![endif]-->

    </head>


    <body>

    <!-- Loader -->
    <div class="fh5co-loader"></div>

    <div id="fh5co-page">
        <section id="fh5co-header">
            <div class="container">
                <nav role="navigation">
                    <ul class="pull-left left-menu">
                        <!-- <li><a href="about.html">About</a></li> -->
                    </ul>
                    <h1 id="fh5co-logo"><a href="index.html">jernlib</a></h1>
                    <ul class="pull-right right-menu">
                        <li><a href="<?php echo base_url('auth/login'); ?>">Login</a></li>
                        <li class="fh5co-cta-btn"><a href="<?php echo base_url('auth/register'); ?>">Sign up</a></li>
                    </ul>
                </nav>
            </div>
        </section>
        <!-- #fh5co-header -->

        <section id="fh5co-hero" class="js-fullheight" style="background-image: url(<?php echo base_url('images/dmitrij-paskevic-44124.jpg'); ?>);" data-next="yes">
            <div class="fh5co-overlay"></div>
            <div class="container">
                <div class="fh5co-intro js-fullheight">
                    <div class="fh5co-intro-text">
                        <!--
                            INFO:
                            Change the class to 'fh5co-right-position' or 'fh5co-center-position' to change the layout position
                            Example:
                            <div class="fh5co-right-position">
                        -->
                        <div class="fh5co-left-position">
                            <h2 class="animate-box">Explore and share knowledge in limitless library</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fh5co-learn-more animate-box">
                <a href="#" class="scroll-btn">
                    <span class="arrow"><i class="icon-chevron-down"></i></span>
                </a>
            </div>
        </section>
        <!-- END #fh5co-hero -->

        <footer id="fh5co-footer">
            <div class="fh5co-copyright animate-box">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="fh5co-left"><small>&copy; 2016 <a href="<?php echo base_url('/'); ?>">Jernlib</a>. All Rights Reserved.</small></p>
                            <p class="fh5co-right"><small class="fh5co-right">Designed by <a href="http://freehtml5.co" target="_blank">FREEHTML5.co</a></small></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END #fh5co-footer -->
    </div>
    <!-- END #fh5co-page -->


    <!-- jQuery -->
    <script src="<?php echo base_url('js/landing/jquery.min.js'); ?>"></script>
    <!-- jQuery Easing -->
    <script src="<?php echo base_url('js/landing/jquery.easing.1.3.js'); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('js/landing/bootstrap.min.js'); ?>"></script>
    <!-- Waypoints -->
    <script src="<?php echo base_url('js/landing/jquery.waypoints.min.js'); ?>"></script>
    <!-- Flexslider -->
    <script src="<?php echo base_url('js/landing/jquery.flexslider-min.js'); ?>"></script>
    <!-- Magnific Popup -->
    <script src="<?php echo base_url('js/landing/jquery.magnific-popup.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/landing/magnific-popup-options.js'); ?>"></script>

    <!-- Main JS (Do not remove) -->
    <script src="<?php echo base_url('js/landing/main.js'); ?>"></script>

    </body>
</html>


