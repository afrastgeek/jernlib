<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dashboard Controller
 */
class Dashboard extends Jernlib_Controller
{
    protected $access = "@";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $this->load->view('admin/_header');
        $this->load->view('admin/home');
        $this->load->view('admin/_footer');
    }
}
