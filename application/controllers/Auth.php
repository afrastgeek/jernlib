<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Auth Controller
 */
class Auth extends Jernlib_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function logged_in_check()
    {
        if ($this->session->userdata("logged_in")) {
            redirect("dashboard");
        }
    }

    public function login()
    {
        $this->logged_in_check();

        $this->load->library('form_validation');
        $this->form_validation->set_rules("username", "Username", "trim|required");
        $this->form_validation->set_rules("password", "Password", "trim|required");
        if ($this->form_validation->run() == true) {
            $this->load->model('user_model', 'auth');
            // check the username & password of user
            $status = $this->auth->validate();
            if ($status == ERR_INVALID_USERNAME) {
                $this->session->set_flashdata("error", "Username is invalid");
            } elseif ($status == ERR_INVALID_PASSWORD) {
                $this->session->set_flashdata("error", "Password is invalid");
            } else {
                // success
                // store the user data to session
                $this->session->set_userdata($this->auth->get_data());
                $this->session->set_userdata("logged_in", true);
                // redirect to dashboard
                redirect("dashboard");
            }
        }

        $this->load->view("login");
    }

    public function logout()
    {
        $this->session->unset_userdata("logged_in");
        $this->session->sess_destroy();
        redirect("auth/login");
    }
}
