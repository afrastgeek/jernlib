<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Controller
 */
class User extends Jernlib_Controller
{
    protected $access = "Admin";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
    }

    public function index()
    {
        $meta['title'] = "Users";
        $data['users'] = $this->user_model->all();
        $this->load->view('admin/_header', $meta);
        $this->load->view('admin/users', $data);
        $this->load->view('admin/_footer');
    }
}
